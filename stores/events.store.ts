/**
 * A Pinia store for managing events.
 *
 * @constant {StoreDefinition} useEventStore
 * @method on - Adds an event and its corresponding options to the store.
 * @method off - Removes an event from the store.
 * @method emit - Calls the handler function for a given event.
 * @method all - Returns a Map of all events in the store.
 * @method isRegistered - Returns a computed boolean indicating whether an event is registered in the store.
 * @returns {Object} A Pinia store object.
 */
export const useEventStore = definePiniaStore('events', () => {
  // Create a reactive reference to a Map object that will store the events
  const events = ref(new Map<string, IBaseEvent>())

  /**
   * Adds an event and its corresponding options to the store.
   *
   * @param {string} e - The name of the event to add.
   * @param {IBaseEvent} options - The options for the event.
   * @throws {Error} If the event already exists in the store.
   */
  function on(e: string, options: IBaseEvent): void {
    if (events.value.has(e)) throw new Error('Event already exists')
    events.value.set(e, options)
  }

  /**
   * Removes an event from the store.
   *
   * @param {string} e - The name of the event to remove.
   * @returns {boolean} Whether the event was successfully removed.
   */
  function off(e: string): boolean {
    return events.value.delete(e)
  }

  /**
   * Calls the handler function for a given event.
   *
   * @param {string} e - The name of the event to emit.
   * @throws {Error} If the event does not exist in the store.
   */
  function emit(e: string) {
    const event = events.value.get(e)
    if (!event) throw new Error('Event does not exist')
    return event.handler()
  }

  /**
   * Returns a Map of all events in the store.
   *
   * @returns {Ref<Map<string, IBaseEvent>>} A Map of all events in the store.
   */
  function all() {
    return events
  }

  /**
   * Returns a computed boolean indicating whether an event is registered in the store.
   *
   * @param {string} e - The name of the event to check.
   * @returns {ComputedRef<boolean>} A computed boolean indicating whether the event is registered in the store.
   */
  function isRegistered(e: string) {
    return computed(() => events.value.has(e))
  }

  return { on, off, isRegistered, emit, all }
})
