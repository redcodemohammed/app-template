/**
 * A Pinia store for managing routes.
 *
 * @method  registerRoutes - Registers routes for a specific app.
 * @method getRoutes - Gets the routes for a specific app.
 * @returns A Pinia store object.
 */
export const useRoutesStore = definePiniaStore('routes', () => {
  // Create a reactive reference to a Map object that will store the routes for each app
  const routesRef = ref(new Map<string, IBaseRoute[]>())

  /**
   * Registers routes for a specific app.
   *
   * @param {string} app - The name of the app to register routes for.
   * @param {IBaseRoute[] | IBaseRoute} routes - The routes to register for the app.
   */
  function registerRoutes(app: string, routes: IBaseRoute[] | IBaseRoute): void {
    // Get the current routes for the app, or an empty array if none exist
    const appRoutes = routesRef.value.get(app) || []

    // If the routes parameter is an array, push all routes to the appRoutes array using the spread operator
    // Otherwise, push the single route object to the appRoutes array
    Array.isArray(routes) ? appRoutes.push(...routes) : appRoutes.push(routes)

    // Set the appRoutes array back to the routesRef object using the app string as the key
    routesRef.value.set(app, appRoutes)
  }

  /**
   * Gets the routes for a specific app.
   *
   * @param {string} app - The name of the app to get routes for.
   * @returns {IBaseRoute[]} The routes for the app.
   * @throws {Error} If no routes are found for the app.
   */
  function getRoutes(app: string): IBaseRoute[] {
    // Get the routes for the app from the routesRef object
    const appRoutes = routesRef.value.get(app)

    // If no routes are found for the app, throw an error
    if (!appRoutes) {
      throw new Error(`Routes for app ${app} not found`)
    }

    // Return the routes for the app
    return appRoutes
  }

  // Return the registerRoutes and getRoutes functions as part of the Pinia store
  return { registerRoutes, getRoutes }
})
