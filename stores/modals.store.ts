import { VDialog } from 'vuetify/components/VDialog'

export const useModalsStore = definePiniaStore('modals', () => {
  // Create reactive references to the showingModal and modalComponent values
  const showingModal = ref(false)
  const modalComponent = shallowRef<Component | null>(null)
  const modalProps = ref()

  /**
   * Shows a modal with the specified component and props.
   *
   * @param {Component} component - The component to use for the modal.
   * @param {Object} props - The props to pass to the component.
   */
  function showModal(component: Component, props: VDialog['$props']) {
    modalComponent.value = component
    modalProps.value = props

    showingModal.value = true
  }

  /**
   * Hides the currently showing modal.
   */
  function hideModal() {
    showingModal.value = false
  }

  /**
   * Closes the currently showing modal.
   */
  function closeModal() {
    modalComponent.value = null
    showingModal.value = false
  }

  return { showModal, hideModal, closeModal, showingModal, modalComponent, modalProps }
})
