import { Nuxt } from 'nuxt/schema'

export function installApp({ events, name, routes }: InstallAppOptions) {
  const $events = useEvents()
  const $routes = useRoutes()

  $events.on(events)
  $routes.registerRoutes(name, routes)
}
