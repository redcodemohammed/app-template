import { useEventStore } from '~/stores/events.store'

export default function () {
  const eventsStore = useEventStore()

  return {
    /**
     * Register one or more events.
     */
    on(events: IBaseEvent | IBaseEvent[]) {
      if (Array.isArray(events)) {
        events.forEach((event) => {
          eventsStore.on(event.name, event)
        })
      } else {
        eventsStore.on(events.name, events)
      }
    },
    /**
     * Emit an event.
     */
    emit(name: string) {
      if (eventsStore.isRegistered(name)) {
        eventsStore.emit(name)
      } else {
        console.warn(`Event ${name} was not registered.`)
      }
    },
    /**
     * Get all registered events.
     */
    all(): Ref<Map<string, IBaseEvent>> {
      return eventsStore.all()
    },
    /**
     * Determine if an event is registered.
     */
    isRegistered(name: string): ReturnType<typeof eventsStore.isRegistered> {
      return eventsStore.isRegistered(name)
    },
    /**
     * Unregister an event.
     */
    off(name: string) {
      eventsStore.off(name)
    }
  }
}
