import { useRoutesStore } from '~/stores/routes.store'

export default function () {
  const { getRoutes, registerRoutes } = useRoutesStore()

  return {
    getRoutes,
    registerRoutes
  }
}
