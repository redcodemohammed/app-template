export { IBaseEvent, IBaseRoute, InstallAppOptions }

declare global {
  interface IBaseEvent {
    /**
     * Name of the event it will be scoped to the app name
     * @example
     * "onCreate"
     * This will be registered as "appName:onCreate"
     */
    name: string
    /**
     * Handler of the event
     * @example
     * () => alert('create')
     */
    handler: () => any
    /**
     * App name of the event
     * @example
     * "home"
     */
    app: string
  }

  export interface IBaseRoute {
    /**
     * Name of the route
     * @example
     * "Home"
     */
    name: string
    /**
     * Path of the route
     * @example
     * "/home"
     */
    to: string
    /**
     * Icon of the route
     * @example
     * "mdi-home"
     */
    icon: string
  }

  interface InstallAppOptions {
    name: string
    app: nuxt
    events: IBaseEvent[] | IBaseEvent
    routes: IBaseRoute[] | IBaseRoute
  }
}
